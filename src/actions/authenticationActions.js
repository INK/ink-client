import * as types from '../constants/ActionTypes';
import { updateAuthHeaders } from './actions_helper.js';
import { getAllRecipesWithTokens } from './recipeActions';
import { browserHistory } from 'react-router';
import { setAlert } from './actions_helper.js';

import _ from 'lodash';
import thunk from 'redux-thunk';
import settings from '../../settings';

const api_headers = {
  'Content-Type': 'application/json',
	'Accept': settings.apiVersionHeader
};

export function fetchSignIn(email, password, appState) {
  let theResponse;

  return function(dispatch) {
		dispatch({type: types.SIGN_IN_REQUEST});

    fetch(`${settings.apiBaseUrl}/api/auth/sign_in`, {
      method: 'POST',
      headers: api_headers,
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => {
      theResponse = response;
      return(response.json());
    })
    .then(json => {
      if(_.isUndefined(json.data)) {
        let error = new Error(theResponse.statusText);
        error.response = json.errors;
        throw error;
      }
      else {
        dispatch(setAlert(["Welcome to INK"], "success"));
        dispatch(successSignIn(json));
        dispatch(updateAuthHeaders(theResponse));
      }
      return(json);
    })
    .then(json => {
      browserHistory.push('/');
      getAllRecipesViaResponse(dispatch, theResponse);
    })
		.catch(error => {
      dispatch(setAlert("Unable to sign you in. Check and try again."));
			dispatch(rejectSignIn(dispatch, error.response));
    });
		return null;
  };
}

export function getAllRecipesViaResponse(dispatch, response) {
  let authToken = response.headers.get('Access-Token');
  let tokenType = response.headers.get('Token-Type');
  let client = response.headers.get('Client');
  let expiry = response.headers.get('Expiry');
  let uid = response.headers.get('uid');
  dispatch(getAllRecipesWithTokens(true, authToken, tokenType, client, expiry, uid));
}

export function successSignIn(json) {
  return {
    type: types.SIGN_IN_SUCCESS,
    account: json.data.account
  };
}

export function rejectSignIn(dispatch, error) {
  return {
    type: types.SIGN_IN_FAILURE
  };
}

/////////////////////////////////
////////// sign out /////////////
/////////////////////////////////

export function fetchSignOut(appState) {
  return function(dispatch) {
		dispatch({type: types.SIGN_OUT_REQUEST});

    const { authToken, tokenType, client, expiry, uid } = appState.session;

    fetch(`${settings.apiBaseUrl}/api/auth/sign_out`, {
      method: 'DELETE',
      headers: {
        'Accept': settings.apiVersionHeader,
        'Content-Type': 'application/json',
        'Access-Token': authToken,
        'Client': client,
        'Token-Type': tokenType,
        'Expiry': expiry,
        'uid': uid
      }
    })
    .then(response => {
      dispatch(
        updateAuthHeaders(response)
      );
      return response.json();
    })
		.then(json => {
      dispatch(setAlert(["You're all signed out"], "success"));
      dispatch(successSignOut(json));
    })
		.catch(error => {
			dispatch(rejectSignOut(error));
    });
		return null;
  };
}

export function successSignOut() {
  return {
    type: types.SIGN_OUT_SUCCESS
  };
}

export function rejectSignOut() {
  return {
    type: types.SIGN_OUT_FAILURE
  };
}
