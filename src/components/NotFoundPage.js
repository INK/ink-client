import React from 'react';
import { Link } from 'react-router';

const NotFoundPage = () => {
  return (
    <div>
      <h4>
        404 Page Not Found
      </h4>
      <p>Not sure what you clicked on, but you might want to go back.</p>
      <Link to="/"> Go back to homepage </Link>
    </div>
  );
};

export default NotFoundPage;
