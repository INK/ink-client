import React, { Component, PropTypes } from 'react';
import * as actions from '../actions/authenticationActions';
import _ from 'lodash';
import ParameterFormPosition from './recipe-steps/ParameterFormPosition';

export class RecipeStepDetail extends Component {

  renderStepDescription(description) {
    if(_.isEmpty(description)) {
      return(<span className="small-info-text">Cannot find the description for this step class! Are you sure it is spelled correctly?</span>);
    }
    return(description);
  }

  stepShorthand(stepName) {
    let array = stepName.split("::");
    return array[array.length - 1];
  }

  renderStepShorthand(stepName) {
    return(<div><span>{this.stepShorthand(stepName)}</span></div>);
  }

  allStepNameParts(stepName) {
    let array = stepName.split("::");
    if(array[0] == "InkStep") {
      array.shift();
    }
    return array;
  }

  renderStepSummary(stepName) {
    const { human_readable_name } = this.props.recipeStep;

    let array = this.allStepNameParts(stepName);
    return(
      <div>
        <div className="step-module-container">
          { array.map((bit, index) => {
            return(<span key={index} className="step-module">{bit}</span>);
          })}
        </div>
        <div className="step__title">{human_readable_name}</div>
      </div>
    );
  }

  render() {
    const {position, step_class_name, description, execution_parameters} = this.props.recipeStep;

    let full_step_class_name = step_class_name;
    let { recipeStep } = this.props;

    return(
      <div className="individual-step-container">
        <div className="step-basic-info">
          <div className="step step--blue step__number">{position}</div>
          <div className="step">{this.renderStepSummary(step_class_name)}</div>
          <div className="step step--blue step__description">{this.renderStepDescription(description)}</div>
        </div>
        <div className="step step__execution-parameters parameter-list-container">
          <ParameterFormPosition recipeStep={recipeStep}/>
        </div>
      </div>
    );
  }
}

RecipeStepDetail.propTypes = {
  recipeStep: PropTypes.object
};

export default RecipeStepDetail;
