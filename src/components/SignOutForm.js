import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/authenticationActions';
import { browserHistory } from 'react-router';

export class SignOutForm extends React.Component {

  handleSignOut = (e) => {
    const { dispatch, appState } = this.props;
    dispatch(actions.fetchSignOut(appState));
    browserHistory.push('/signin');
  }

  render() {
    return (
      <span>&#124; <a href="#" className="navbar-link" onClick={this.handleSignOut}>Sign Out</a></span>
    );
  }
}

SignOutForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignOutForm);
