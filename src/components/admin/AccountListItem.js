import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import * as actions from '../../actions/adminActions';

import _ from 'lodash';

export class AccountListItem extends Component {

  renderAdmin(admin) {
    if(admin == true) {
      return(<span><span className="fa fa-pie-chart" /> </span>);
    } else {
      return(null);
    }
  }

  renderAccountDetails(account) {
    return(
      <span>
        <span>{this.renderAdmin(account.admin)}</span>
        {account.name} {account.email}
      </span>
    );
  }

  render() {
    let { account } = this.props;

    return(
      <div className="account-item">
        <Link to={`/admin/accounts/${account.id}`}>
          {this.renderAccountDetails(account)}
        </Link>
      </div>
    );
  }
}

AccountListItem.propTypes = {
  account: PropTypes.object.isRequired
};

export default AccountListItem;
