import React, {PropTypes} from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import validate from './validate';
import { connect } from 'react-redux';

export class ParameterArrayForm extends React.Component {

  required = value => (value ? undefined : 'Required');
  noWhitespace = value => (value.match(".*\\s+.*") ? 'Whitespace not permitted' : undefined);

  errorIcon (touched, error) {
    if(touched && error) {
      return(<span className="fa fa-exclamation-triangle red" title={error}/>);
    }
    return null;
  }

  renderField = ({ input, label, type, meta: { touched, error } }) =>
    <div className="input-wide-container">
      <input {...input} type={type} placeholder={error && label} className={`${touched && error && "input-error"} input-wide`} />
      {this.errorIcon(touched, error)}
    </div>

  removeLink = () =>
    <span><span className="fa fa-times"/> Remove</span>

  addLink = () =>
    <span><span className="fa fa-plus"/> <span className="fa fa-key"/> New</span>

  keyIcon = () =>
    <span><span className="fa fa-key"/></span>

  valueIcon = () =>
    <span><span className="fa fa-pencil"/></span>

  renderParameters = ({ fields, meta: { error, submitFailed } }) =>
    <ul>
      <li>
        {submitFailed &&
          error &&
          <span>
            {error}
          </span>}
      </li>
      {fields.map((parameter, index) =>
        <li key={index}>
          <Field
            name={`${parameter}.key`}
            type="text"
            label="key"
            validate={[this.required, this.noWhitespace]}
            component={this.renderField}
            className="asdf"
          />
          <Field
            name={`${parameter}.value`}
            type="text"
            label="value"
            validate={[this.required, this.noWhitespace]}
            component={this.renderField}
          />
          <div className="right-align">
            <button
              type="button"
              title="remove"
              onClick={() => fields.remove(index)}
              className="inline-action-button--blue right-align"
            >{this.removeLink()}</button>
          </div>
        </li>
      )}
        <button className="inline-action-button" type="button" onClick={() => fields.push({})}>
          {this.addLink()}
        </button>
    </ul>

  render() {
    const { handleSubmit, pristine, reset, submitting, invalid, initialValues } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <FieldArray name="parameters" component={this.renderParameters} />
        <div className="right-align">
          <button type="button" className="inline-action-button" disabled={pristine || submitting} onClick={reset}>
            <span className="fa fa-mail-reply" /> Reset to step default
          </button>
        </div>
      </form>
    );
  }
}

ParameterArrayForm.propTypes = {
  appState: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  initialValues: PropTypes.object.isRequired,
  reset: PropTypes.func.isRequired
};

function mapStateToProps(state, props) {
  return {
    appState: state.appState,
    form: `executionParameterForm_${props.recipeStep.position}`
  };
}

export default connect(mapStateToProps)(reduxForm({ enableReinitialize: true }, validate)(ParameterArrayForm));
