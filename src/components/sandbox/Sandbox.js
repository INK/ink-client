import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import { subscribe, unsubscribe } from 'pusher-redux';
import * as eventActions from '../../constants/PusherActions';
import * as actions from '../../actions/recipeActions.js';
import { setAlert, clearAlerts } from '../../actions/actions_helper';

import SyntaxHighlighter, { registerLanguage } from 'react-syntax-highlighter/light';
import { tomorrowNightEighties } from 'react-syntax-highlighter/styles/prism';
// import ruby from 'react-syntax-highlighter/languages/prism/ruby';
// import SyntaxHighlighter, { registerLanguage } from "react-syntax-highlighter/light";
import ruby from 'react-syntax-highlighter/languages/hljs/ruby';

import CodeForm from './CodeForm.js';
import ExecutionDetail from './ExecutionDetail';
import ExecutionList from './ExecutionList';

registerLanguage('ruby', ruby);

export class Sandbox extends Component {

  constructor(props, context) {
    super(props, context);
    this.subscribe = this.subscribe.bind(this);
    this.unsubscribe = this.unsubscribe.bind(this);
   }

  componentWillMount = () => {
    let { dispatch, appState } = this.props;
    const { session } = appState;

    dispatch(clearAlerts());

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

    this.subscribe();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  subscribe() {
    let account_id = this.props.appState.session.account.id;
    let channel = `${account_id}_single_step_execution`;

    console.log("subscribed to", channel)

    subscribe(channel, 'single_step_execution_created', eventActions.SINGLE_STEP_EXECUTION_CREATED);
    subscribe(channel, 'single_step_execution_started', eventActions.MARK_SINGLE_STEP_EXECUTION_AS_STARTED);
    subscribe(channel, 'single_step_execution_completed', eventActions.MARK_SINGLE_STEP_EXECUTION_AS_COMPLETED);
    subscribe(channel, 'single_step_execution_processing_error', eventActions.MARK_SINGLE_STEP_EXECUTION_AS_ERRORED);
  }

  unsubscribe() {
    let account_id = this.props.appState.session.account.id;
    let channel = `${account_id}_single_step_execution`;

    unsubscribe(channel, 'single_step_execution_created', eventActions.SINGLE_STEP_EXECUTION_CREATED);
    unsubscribe(channel, 'single_step_execution_started', eventActions.MARK_SINGLE_STEP_EXECUTION_AS_STARTED);
    unsubscribe(channel, 'single_step_execution_completed', eventActions.MARK_SINGLE_STEP_EXECUTION_AS_COMPLETED);
    unsubscribe(channel, 'single_step_execution_processing_error', eventActions.MARK_SINGLE_STEP_EXECUTION_AS_ERRORED);
  }

  single_step_execution_creation_channel() {
    let { appState } = this.props;
    return(`${appState.session.account.id}_single_step_execution`);
  }

  handleSubmission(values) {
    const { dispatch, appState } = this.props;

    console.log("values", values)
    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
  }

  renderCodePreview() {
    // TODO: finish implementing.
    return(
      <div className="sandbox-right">
        <h4>Preview</h4>
        <div className="sandbox-right-code-wrapper">
          <textarea>{highlightCode("abc = 123")}</textarea>
        </div>
      </div>
    );
  }

  render() {
    const { dispatch, appState } = this.props;
    return (
      <div className="sandbox-code-wrapper">
        <CodeForm />
        <ExecutionList />
      </div>
    );
  }
}

function highlightCode(codeString) {
  return <SyntaxHighlighter language='ruby' style={tomorrowNightEighties}>{codeString}</SyntaxHighlighter>;
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

Sandbox.propTypes = {
  appState: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps
)(Sandbox);
