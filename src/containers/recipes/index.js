import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import * as actions from '../../actions/authenticationActions';

import Header from '../../components/header/Header';

import RecipesList from '../../components/RecipesList';
import AlertList from '../../components/AlertList';
import { getAllRecipes } from '../../actions/recipeActions.js';
import { setAlert } from '../../actions/actions_helper';

class RecipeIndexPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentWillMount = () => {
    const { appState, dispatch } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

  }

  componentDidMount = () => {
    const { appState, dispatch } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }

    dispatch(getAllRecipes(appState));
  }

  gotoNewRecipe = () => {
    browserHistory.push('/recipes/new');
    return;
  }

  pageContent(appState) {
    if(_.isNull(appState.recipes) || _.isUndefined(appState.recipes)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">loading...</span></div>
        </div>
      );
    }
    else {
      return(
        <div>
          <h1>Available Recipes</h1>
          <h4>Select a recipe or add a new one</h4>
          <div>
            <button className="inline-action-button" onClick={this.gotoNewRecipe}><span className="fa fa-plus"/> Add a new recipe</button>
          </div>
          <RecipesList
            recipes={appState.recipes}
            getRecipesInProgress={appState.getRecipesInProgress}
            session={appState.session}
          />
        </div>
      );
    }

  }

  render() {
    const { appState } = this.props;

    return (
      <div>
        <Header
          appState={appState}
        />
        <div className="content-container">
          <div className="breadcrumb-container">
            <Link to="/" className="breadcrumb">Home</Link>
            <span className="breadcrumb-divider">&gt;&gt;</span>
            <Link to="/recipes" className="breadcrumb"><span className="fa fa-calendar-o"/> Recipes</Link>
          </div>

          <AlertList
            alerts={appState.alerts} />
          {this.pageContent(appState)}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    alerts: PropTypes.array.isRequired,
    recipes: PropTypes.array.isRequired
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeIndexPage);
