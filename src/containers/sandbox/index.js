import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import * as actions from '../../actions/adminActions.js';

import Header from '../../components/header/Header';
import { setAlert } from '../../actions/actions_helper';

import Sandbox from '../../components/sandbox/Sandbox';

class SandboxPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentWillMount = () => {
    this.checkAuth();
  }

  componentDidMount = () => {
    const { appState, dispatch } = this.props;
    this.checkAuth();
    // dispatch(actions.getAllAccounts(appState));
    // dispatch(actions.getStatusReport(appState));
    // dispatch(actions.getAvailableStepGems(appState));
  }

  checkAuth() {
    const { appState, dispatch } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    }
  }

  pageContent() {
    const { appState, dispatch } = this.props;
    return(
      <div className="sandbox-view-container">
        <Sandbox appState={appState} dispatch={dispatch}/>
      </div>
    );
  }

  render() {
    const { appState } = this.props;

    return (
      <div>
        <Header
          appState={appState}
        />
        <div className="content-container">
          <div className="breadcrumb-container">
            <Link to="/" className="breadcrumb">Home</Link>
            <span className="breadcrumb-divider">&gt;&gt;</span>
            <Link to="/admin/accounts" className="breadcrumb"><span className="fa fa-accounts-o"/> Accounts</Link>
          </div>

          <h1>Step Sandbox</h1>
          <h4>Write some code below that could be a useful step, and see the results. You can see how awesome INK is without having to install your own instance.</h4>
          {this.pageContent(appState)}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    alerts: PropTypes.array.isRequired,
    recipes: PropTypes.array.isRequired
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SandboxPage);
