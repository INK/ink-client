import { configurePusher } from 'pusher-redux';
import * as environments from './constants/Environments';

let stage = process.env.STAGE;

export function setupPusher(store) {
  if(stage === environments.PRODUCTION) {
    const config = require("json!yaml!./config/slanger-production.yml");
    const options = {
      appKey: config.app_key,
      httpHost: config.http_host,
      httpPort: config.http_port,
      wsHost: config.web_host,
      wsPort: config.web_port
    };
    console.log(`configuring pusher for ${stage}:`, options);

    configurePusher(store, config.app_key, options);
  } else if(stage === environments.STAGING) {
    const config = require("json!yaml!./config/slanger-staging.yml");
    const options = {
      appKey: config.app_key,
      httpHost: config.http_host,
      httpPort: config.http_port,
      wsHost: config.web_host,
      wsPort: config.web_port
    };
    console.log(`configuring pusher for ${stage}:`, options);

    configurePusher(store, "inkstaging123", options);
  } else if(stage === environments.DEMO) {
    const config = require("json!yaml!./config/slanger-demo.yml");
    const options = {
      appKey: config.app_key,
      httpHost: config.http_host,
      httpPort: config.http_port,
      wsHost: config.web_host,
      wsPort: config.web_port
    };
    console.log(`configuring pusher for ${stage}:`, options);

    configurePusher(store, "inkdemo123", options);
  } else if(stage === environments.DEVELOPMENT || process.env.NODE_ENV === environments.DEVELOPMENT) {
    const config = require("json!yaml!./config/slanger.yml");
    const options = {
      httpHost: "localhost",
      httpPort: config.http_port,
      wsHost: "localhost",
      wsPort: config.web_port
    };
    console.log(`configuring pusher for ${environments.DEVELOPMENT}:`, options);

    configurePusher(store, "44332211ffeeddccbbaa", options);
  } else {
    console.log("No environment config available for Slanger (pusher) on environment:", stage);
  }
}
