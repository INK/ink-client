import * as environments from '../constants/Environments';

if(process.env.NODE_ENV === environments.DEVELOPMENT) {
  module.exports = require('./configureStore.dev');
} else {
  module.exports = require('./configureStore.prod');
}
